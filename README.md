# python-csv-to-influxdb
Script to transfer data from csv files to an InfluxDB server (https://www.influxdata.com/)

**Requirements:**  
* Python>=2.7.11
* pytz>=2016.10
* influxdb>=4.0.0
* jsonschema>=2.6.0

**Usage:** 
<pre>python3 bridge.py -c conf/example.json</pre>
